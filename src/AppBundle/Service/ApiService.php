<?php
/**
 * Created by PhpStorm.
 * User: GarethChidgey
 * Date: 23/02/2016
 * Time: 20:19
 */

namespace AppBundle\Service;

use AppBundle\Entity\Company;
use AppBundle\Entity\Director;
use AppBundle\Entity\StockQuote;
use AppBundle\Exception\ServiceException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

/**
 * Class ApiService
 * @package AppBundle\Service
 */
class ApiService
{
    /** @var \SoapClient $soapClient  */
    private $soapClient;

    /** @var \stdClass $soapAuth */
    private $soapAuth;

    /** @var EntityManager $entityManager */
    private $entityManager;

    /**
     * ApiService constructor.
     *
     * @param EntityManager $entityManager
     * @param $apiEndpoint
     * @param $username
     * @param $password
     */
    public function __construct(EntityManager $entityManager, $apiEndpoint, $username, $password)
    {
        $this->entityManager = $entityManager;

        $this->soapAuth = new \stdClass();
        $this->soapAuth->username = $username;
        $this->soapAuth->password = $password;

        $this->soapClient = new \SoapClient($apiEndpoint, [
            'trace' => true,
            'authorisation' => $this->soapAuth
        ]);
    }

    /**
     * Fetch the companies form the SOAP API and parse them into Company Objects
     * Persist them locally at this stage
     *
     * @return Company[]|ArrayCollection
     * @throws ServiceException
     */
    public function getCompanies()
    {
        try {
            $companies = $this->soapClient->getCompanies($this->soapAuth);

            foreach ($companies as $companyObject) {
                $company = $this->entityManager
                    ->createQueryBuilder()
                    ->select('c')
                    ->from('AppBundle:Company', 'c')
                    ->where('c.symbol = :symbol')
                    ->setParameter('symbol', $companyObject->symbol)
                    ->getQuery()
                    ->getOneOrNullResult();

                if (is_null($company)) {
                    $company = new Company($companyObject);
                    $this->entityManager->persist($company);
                }

                //todo modulus batch at ~50
            }

            $this->entityManager->flush();

            $companyCollection = $this->entityManager->getRepository('AppBundle:Company')->findAll();

        } catch (\Exception $e) {
            // I like to know where in my service layer thing any unexpected error is being called
            throw new ServiceException(
                'Error in ' . __CLASS__ . '->' . __FUNCTION__ . ' on line ' . __LINE__,
                $e->getCode(),
                $e
            );
        }

        return $companyCollection;
    }

    /**
     * @param Company $company
     * @return Director[]|ArrayCollection
     * @throws ServiceException
     */
    public function getCompanyDirectorsByCompany(Company $company)
    {
        try {
            $directors = $this->soapClient->getCompanyDirectorsBySymbol($this->soapAuth, $company->getSymbol());

            foreach ($directors as $directorObject) {
                $director = $this->entityManager
                    ->createQueryBuilder()
                    ->select('d')
                    ->from('AppBundle:Director', 'd')
                    ->where('d.company :company')
                    ->setParameter('company', $company)
                    ->andWhere('d.name :name')
                    ->setParameter('name', $directorObject->name)
                    ->getQuery()
                    ->getOneOrNullResult();

                if (is_null($director)) {
                    $director = new Director($directorObject);

                    $this->entityManager->persist($director);

                    //todo modulus batch at ~50
                }
            }

            $this->entityManager->flush();

            $directorCollection = $this->entityManager
                ->createQueryBuilder()
                ->select('d')
                ->from('AppBundle:Director', 'd')
                ->where('d.company :company')
                ->setParameter('company', $company)
                ->getQuery()
                ->getResult();

        } catch (\Exception $e) {
            // I like to know where in my service layer thing any unexpected error is being called
            throw new ServiceException(
                'Error in ' . __CLASS__ . '->' . __FUNCTION__ . ' on line ' . __LINE__,
                $e->getCode(),
                $e
            );
        }

        return $directorCollection;
    }

    /**
     * @param Company $company
     * @return StockQuote
     * @throws ServiceException
     */
    public function getQuote(Company $company)
    {
        try {
            $quoteResponse = $this->soapClient->getQuote($this->soapAuth, $company->getSymbol());

            $quote = new StockQuote($quoteResponse, $company);

            $this->entityManager->persist($quote);
            $this->entityManager->flush();

        } catch (\Exception $e) {
            // I like to know where in my service layer thing any unexpected error is being called
            throw new ServiceException(
                'Error in ' . __CLASS__ . '->' . __FUNCTION__ . ' on line ' . __LINE__,
                $e->getCode(),
                $e
            );
        }

        return $quote;
    }
}
