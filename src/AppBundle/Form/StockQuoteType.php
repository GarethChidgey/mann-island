<?php

namespace AppBundle\Form;

use AppBundle\Service\ApiService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StockQuoteType extends AbstractType
{
    /** @var ApiService $apiService  */
    private $apiService;

    /**
     * StockQuoteType constructor.
     * @param ApiService $apiService
     */
    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\StockQuote'
        ]);
    }

    public function getName()
    {
        return 'app_bundle_stock_quote_type';
    }
}
