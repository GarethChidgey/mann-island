<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="symbol", type="string", length=100)
     */
    private $symbol;

    /**
     * @var StockQuote[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="StockQuote",
     *     mappedBy="company",
     *     cascade={"persist"},
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true
     * )
     */
    private $stockQuotes;

    /**
     * @var Director[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="Director",
     *     mappedBy="company",
     *     cascade={"persist"},
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true
     * )
     */
    private $directors;

    /**
     * Company constructor.
     * @param \stdClass $company
     */
    public function __construct(\stdClass $company = null)
    {
        if (!empty($company)) {
            $this->setName($company->name);
            $this->setSymbol($company->symbol);
        }

        $this->stockQuotes = new ArrayCollection();
        $this->directors = new ArrayCollection();
    }

    /**
     * @return string
     */
    function __toString()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Company
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     * @return Company
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
        return $this;
    }

    /**
     * @return StockQuote[]|ArrayCollection
     */
    public function getStockQuotes()
    {
        return $this->stockQuotes;
    }

    /**
     * @param StockQuote[]|ArrayCollection $stockQuotes
     * @return Company
     */
    public function setStockQuotes($stockQuotes)
    {
        $this->stockQuotes = $stockQuotes;
        return $this;
    }

    /**
     * @return Director[]|ArrayCollection
     */
    public function getDirectors()
    {
        return $this->directors;
    }

    /**
     * @param Director[]|ArrayCollection $directors
     * @return Company
     */
    public function setDirectors($directors)
    {
        $this->directors = $directors;
        return $this;
    }
}
