<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * StockQuote
 *
 * @ORM\Table(name="stock_quote")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StockQuoteRepository")
 */
class StockQuote
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="quote", type="float")
     */
    private $quote;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(
     *     targetEntity="Company",
     *     inversedBy="stockQuotes",
     *     cascade={"persist"},
     *     fetch="EXTRA_LAZY"
     * )
     * @ORM\JoinColumn(
     *     name="company_id",
     *     referencedColumnName="id",
     *     unique=false,
     *     nullable=false
     * )
     */
    private $company;

    /**
     * StockQuote constructor.
     * @param null $quote
     * @param Company|null $company
     */
    public function __construct($quote = null, Company $company = null)
    {
        if (!is_null($quote) && !is_null($company)) {
            $this->setDate(new \DateTime());
            $this->setCompany($company);
            $this->setQuote($quote);
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return StockQuote
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return float
     */
    public function getQuote()
    {
        return $this->quote;
    }

    /**
     * @param float $quote
     * @return StockQuote
     */
    public function setQuote($quote)
    {
        $this->quote = $quote;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return StockQuote
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     * @return StockQuote
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }
}
