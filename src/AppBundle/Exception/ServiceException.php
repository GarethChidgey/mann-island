<?php
/**
 * Created by PhpStorm.
 * User: GarethChidgey
 * Date: 23/02/2016
 * Time: 20:43
 */

namespace AppBundle\Exception;

/**
 * Class ServiceException
 * @package AppBundle\Exception
 */
class ServiceException extends \Exception
{
    // TODO - Implement additional logging of API response failures (Deemed outside of the scope of the Tech Test)
}