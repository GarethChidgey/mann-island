<?php

namespace AppBundle\Controller;

use AppBundle\Exception\ServiceException;
use AppBundle\Form\StockQuoteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/", name="app.default.index")
     */
    public function indexAction(Request $request)
    {
        try {
            $apiService = $this->get('app.service.api');
            $quote = null;

            $form = $this->createForm(new StockQuoteType($apiService));

            $form->handleRequest($request);

            if ($form->isValid()) {

                if (!is_null($request->request->get('get_directors', null))) {
                    $apiService->getCompanyDirectorsByCompany($form->getData()->getCompany());
                } else {
                    $quote = $apiService->getQuote($form->getData()->getCompany());
                }
            }

        } catch (ServiceException $e) {
            return $this->render('::error.html.twig', [
                'errorMessage' => $e->getMessage()
            ]);
        }

        return $this->render(':Default:index.html.twig', [
            'form' => $form->createView(),
            'quote' => $quote
        ]);
    }
}
