Mann Island Tech Test
=====================

Gareth Chidgey (23/02/2016)
---------------------------

### Summary

This is a [Symfony 2](https://symfony.com/)  Application which is designed to answer the Tech Test issued.
It has been about a decade since I last used SOAP. I have been using REST since then.

____
### Installation

* Runs on an apache 2.2/2.4 with PHP 5.5+ and MYSQL 5.6+
* Not intended to run on a Windows box
* [PATH_OF_YOUR_CHOICE] should be replaced throughout this with a path in which you want to install the application

#### Files
* In the CLI of your choice
* Run `git clone git@bitbucket.org:GarethChidgey/mann-island.git [PATH_OF_YOUR_CHOICE]`
* Then `cd [PATH_OF_YOUR_CHOICE]`
* Run `composer install` in the site root in a terminal/ bash/ shell

#### Vhost

Configure a Vhost with any dummy or live domain to the web folder with the following:
 
```
Allow From All
AllowOverride All
Options +Indexes
Require all granted
```

The .htaccess will take care of the rest. In production I would not use an .htaccess.

#### MySQL

* Create a blank database and a user with rights to that database
* Set the MySql connection settings in app/config/parameters.yml

* Again in the CLI of your choice
* Run `cd [PATH_OF_YOUR_CHOICE]`
* at the site root run `app/console doctrine:schema:create` to generate the database schema

